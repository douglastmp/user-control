const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcrypt');

const User = require('../models/user.model')

module.exports = function(passport) {
    passport.use(
        new LocalStrategy({usernameField: 'username'}, (username, password, done) => {
            User.findOne({username: username})
            .then(user => {
                if (!user) {
                    return done(null, false, {message: 'Usuário não cadastrado!'})
                }
                else {
                    bcrypt.compare(password, user.password, (err, isMatch) => {
                        if (err) throw err;

                        if (isMatch) {
                            return done(null, user);
                        } else{
                            return done(null, false, {message: 'Senha incorreta!'})
                        }
                    })
                }
            })
            .catch(err => console.log('Error: '+ err))
        })
    );

    passport.serializeUser((user, done) => {
        done(null, user.id);
      });
      
    passport.deserializeUser((id, done) => {
        User.findById(id, (err, user) => {
          done(err, user);
        });
      });
}