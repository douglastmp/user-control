import React from 'react'
import {Link} from 'react-router-dom'
import axios from '../services/axios'
import history from '../history'
import * as actionTypes from '../store/actions'
import {connect} from 'react-redux'

const NavBar = (props) => {
    const exitHandle = () => {
        axios.get('/logout')
        .then(res => {if (res.status === 200) {
            props.onLogout();
            history.push('/');
          }})
        .catch(err => console.log('Error: ' + err))
        
    }

    return (
        <div>
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <div className="collapse navbar-collapse container" id="navbarSupportedContent">
                    <ul className="mr-auto navbar-nav ">
                        <li className="nav-item col-12 text-left">
                            <Link to="/" className="nav-link">User Control</Link>
                        </li>
                    </ul>
                    <ul className="navbar-nav mx-auto text-center">
                        <li className="nav-item">
                            <Link to="/users" className="nav-link">Users</Link>
                        </li>
                        <li className="nav-item">
                            <Link to="/create" className="nav-link">Create User</Link>
                        </li>  
                    </ul>
                    <ul className="navbar-nav mx-auto">
                        <li className="nav-item">
                            {props.auth ? <Link onClick={exitHandle} to="/logout" className="nav-link">Sair</Link>: <div></div>}
                        </li>  
                    </ul>
                </div>
            </nav>
        </div>
    )
}


const mapStatetoProps = state => {
    return {
        auth: state.isAuth
    };
}

const mapDispatchToProps = dispatch => {
    return {
        onLogout: () => dispatch({type: actionTypes.LOGOUT})
    }
};

export default connect(mapStatetoProps, mapDispatchToProps)(NavBar);