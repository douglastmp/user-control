const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const cookieParser = require('cookie-parser');
const passport = require('passport');
const session = require('express-session');
const MongoStore = require('connect-mongo')(session)


//Initializing Server
require('dotenv').config();
const app = express();
const whitelist = ['http://localhost:3000', 'http://example2.com'];
const corsOptions = {
  credentials: true, // This is important.
  origin: (origin, callback) => {
    if(whitelist.includes(origin))
      return callback(null, true)

      callback(new Error('Not allowed by CORS'));
  }
}


//Passport Config
require('./config/passport')(passport);

app.use(express.static(__dirname));
app.use(cors(corsOptions));
app.use(express.json());
app.use(cookieParser());


//Connecting to DB
const uri = process.env.ATLAS_URI;
mongoose.connect(uri, {useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true});

const connection = mongoose.connection;
connection.once('open', () => {
    console.log('MongoDB database connected')
})

// Express session
app.use(
  session({
    secret: 'secret',
    resave: true,
    saveUninitialized: true,
    store: new MongoStore({
      url: process.env.ATLAS_URI,
      collection: 'sessions'
    })
  })
);

//passport Middleware
app.use(passport.initialize());
app.use(passport.session());

//Routing
const usersRouter = require('./routes/users');
const authRouter = require('./routes/auth');
app.use('/', authRouter);
app.use('/users', usersRouter);
app.get('/checkToken', function(req, res) {
    res.sendStatus(200);
  }
);

const port = process.env.PORT || 5000;
//Runnning server
app.listen(port, () => {
    console.log(`Server is running on port: ${port}`);
});