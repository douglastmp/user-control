import React from 'react';
import './App.css';
import {Router, Route} from 'react-router-dom'
import UserList from './components/UserList'
import UserCreate from './components/UserCreate'
import UserEdit from './components/UserEdit'
import Login from './components/Login'
import NavBar from './components/Navbar'
import history from './history'

function App() {
  return (
    <div className="App">
      <header>
        <Router history={history}>
          <NavBar />
          <Route path="/users/:id" exact component={UserEdit}/>
          <Route path="/users" exact component={UserList}/>
          <Route path="/create" exact component={UserCreate}/>
          <Route path="/" exact component={Login}/>
        </Router>
      </header>
    </div>
  );
}

export default App;
