const router = require('express').Router();
const ensureAuth = require('../middleware/token')
let User = require('../models/user.model');

//create
router.route('/add').post((req, res) => {
    const { username, password } = req.body;
 
    const newUser = new User({username, password});

    newUser.save()
        .then(() => res.json('User added!'))
        .catch(err => res.status(400).json('Error: ' + err));
});

//get
router.route('/').get(ensureAuth, (req, res) => {
    User.find()
    .then(users => res.json(users))
    .catch(err => res.status(400).json('Error: '+ err));
});

//get by id
router.route('/:id').get(ensureAuth, (req, res) => {
    User.findById(req.params.id)
        .then(user => res.json(user))
        .catch(err => res.status(400).json('Error: ' + err));
});

//update by id
router.route('/update/:id').post(ensureAuth, (req, res) => {
    User.findById(req.params.id)
        .then(user => {
            user.username = req.body.username
            user.password = req.body.password

            user.save()
            .then(() => res.json(`User ${user.id} updated!`))
            .catch(err => res.status(400).json('Error: ' + err));
        })
        .catch(err => res.status(400).json('Error: ' + err));
});

//delete by id
router.route('/:id').delete(ensureAuth, (req, res) => {
    User.findByIdAndDelete(req.params.id)
        .then(() => res.json('User deleted!'))
        .catch(err => res.status(400).json('Error: ' + err));
});

module.exports = router;