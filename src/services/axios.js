import axios from 'axios';

const instance = axios.create({
    baseURL: 'http://localhost:5000/',
    transformRequest: [(data) => JSON.stringify(data)],
    withCredentials: true,
    headers: {
        'Access-Control-Allow-Origin': '*',
        'Accept': 'application/json',
        'Content-Type': 'application/json',
    }
});

export default instance;