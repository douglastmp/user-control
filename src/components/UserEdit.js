import React, {useRef, useEffect} from 'react'
import Axios from '../services/axios'

const UserCreate = (props) => {
    let user = useRef(null);
    let pass = useRef(null);
    let currentId = props.match.params.id
    
    useEffect(() => {    
        Axios.get(`users/${currentId}`)
            .then(
                response => {
                    user.current.value = response.data.username
                    pass.current.value = response.data.password
                }
            )
    }, [])

    const submitHandle = (event) => {
        event.preventDefault();
        let confirma = window.confirm(`Deseja alterar informações?`);
        if (confirma) {
            let postData = {
                "username": user.current.value,
                "password": pass.current.value,
            }
            Axios.post(`users/update/${currentId}`, postData).then(
                (response) => {
                    alert('Informações alteradas com sucesso!');
                }
            ).catch(
                (error) => {
                    console.log('Error: ' + error)
                }
            )
        }
    }

    return (
        <div className="container mt-5 text-left">
            <form onSubmit={submitHandle}>
                <div className="form-group">
                    <label htmlFor="exampleFormControlInput1">Nome do Usuário:</label>
                    <input type="text" ref={user} className="form-control" placeholder="Francisco" required/>
                </div>
                <div className="form-group">
                    <label htmlFor="exampleFormControlInput1">Senha:</label>
                    <input type="password" ref={pass} maxLength="20" className="form-control" required/>
                </div>
                <div className="col text-center">
                    <input className="btn btn-primary" type="submit" value="Alterar Informações" />
                </div>
            </form>            
        </div>
    )
}

export default UserCreate