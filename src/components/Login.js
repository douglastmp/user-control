import React, {useRef, useEffect} from 'react'
import axios from '../services/axios'
import history from '../history'
import * as actionTypes from '../store/actions'
import { connect } from 'react-redux'

const Login = (props) => {
    let user = useRef(null)
    let pass = useRef(null)

    useEffect(() => {
        if (props.auth) {
            history.push('/users')
        }
    }, [])

    const LoginHandler = (event) => {
        event.preventDefault();
        if(user.current.value && pass.current.value){
            axios.post('/authenticate', {username:user.current.value,
                                         password:pass.current.value})
            .then(res => {
                if (res.status === 200) {
                  props.onLogin();
                  history.push('/users');
                } else {
                  const error = new Error(res.error);
                  throw error;
                }
              })
              .catch(err => {
                console.error(err);
                alert('Error logging in please try again');
              });
        }
    }

    return (
        <div className="container h-100">
            <form className="form-group">
                <div className="justify-content-center">
                    <div className="mr-auto">
                        <input ref={user} className='col col-4 form-control mx-auto mt-5' type="text" placeholder="Username" required></input>
                        <input ref={pass} className='col col-4 form-control mx-auto mt-4' type="password" placeholder="Password" required></input>
                        <button type="button" onClick={LoginHandler} className="btn btn-primary mt-4">Login</button>
                    </div>
                </div>
            </form>
        </div>
    )
}

const mapStatetoProps = state => {
    return {
        auth: state.isAuth
    };
}

const mapDispatchToProps = dispatch => {
    return {
        onLogin: () => dispatch({type: actionTypes.LOGIN})
    }
};

export default connect(mapStatetoProps, mapDispatchToProps)(Login);