import React, {useEffect, useState, useMemo} from 'react'
import axios from '../services/axios'
import UserCard from './UserCard';
import history from '../history'

const UserList = () => {
    const [users, setUsers] = useState();
        
    useEffect(() => {
        axios.get('users')
        .then(response => setUsers(response.data))
        .catch(err => {
            console.log('Error: '+ err)
            history.push('/')
        })
    }, []);

    return (
        <div className="container mt-5">
            {useMemo(() => users ? <UserCard registers={users}/> : <div></div>, [users])}
        </div>
    )
}

export default UserList