import React, {useRef} from 'react'
import Axios from '../services/axios'

const UserCreate = () => {
    let user = useRef(null)
    let pass = useRef(null)

    const submitHandle = (event) => {
        event.preventDefault();
        let postData = {
            "username": user.current.value,
            "password": pass.current.value,
        }
        Axios.post('/users/add/', postData).then(
            (response) => {
                alert('Usuário criado com sucesso!');
            }
        ).catch(
            (error) => {
                console.log('Error: ' + error)
            }
        )
    }

    return (
        <div className="container mt-5 text-left">
            <form onSubmit={submitHandle}>
                <div className="form-group">
                    <label htmlFor="exampleFormControlInput1">Nome do Usuário:</label>
                    <input type="text" ref={user} className="form-control" id="exampleFormControlInput1" placeholder="Francisco" required/>
                </div>
                <div className="form-group">
                    <label htmlFor="exampleFormControlInput1">Senha:</label>
                    <input type="password" ref={pass} className="form-control" id="exampleFormControlInput1" required/>
                </div>
                <div className="col text-center">
                    <input className="btn btn-primary" type="submit" value="Criar Usuário" />
                </div>
            </form>            
        </div>
    )
}

export default UserCreate