This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## User Control Project

Aplicativo crud de usuários com autenticação utilizando o Passport.js.
A criação de usuários é permitida por qualquer um porém para listar, alterar e excluir
só é possível através de efetuar o login com algum usuário. Para teste pode ser utilizado 
o Usuário: douglastmp e senha: 123456. 

A parte de backend foi incluída juntamente com este projeto tendo em vista a simplicidade do 
mesmo sabendo que não é a melhor arquitetura.

## BackEnd

Para utilizar o servidor criado em node basta utilizar o seguinte comando na pasta backend: 
### `node run server` 
Runs the app in the development mode using port 5000.<br>
[http://localhost:5000](http://localhost:5000)

## Available Scripts FrontEnd
No projeto é possível executar os seguintes Scripts:

### `npm install`
Runs the instalation of the package.json dependencies

### `npm start`
Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

