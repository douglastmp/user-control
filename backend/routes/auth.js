const router = require('express').Router();
const passport = require('passport')

router.route('/authenticate').post((req, res, next) => {
  passport.authenticate('local', {
    successRedirect:'/users',
    failureRedirect:'/',
    failureFlash: true
  })(req,res,next)
});

router.route('/logout').get((req, res) => {
  req.logout();
  req.session.destroy(
    res.sendStatus(200)
  )
});

module.exports = router;
