import React from 'react'
import {Link} from 'react-router-dom'
import axios from '../services/axios'

const UserCard = (props) => {
    const deleteUserHandle = (id, username) => (event) => {
        let del = window.confirm(`Deseja excluir o usuário ${username}?`)
        if (del) {
            axios.delete(`users/${id}`).then(
                window.location.reload()
            ).catch(
                err => console.log('Error: ' + err)
            )
        }
    }

    return (
        <div className="container">
            {props.registers.map((el, index) => (
                <div key={el._id} className="card text-left m-5">
                    <div className="card-body">
                        <p className="card-body m-0 p-1"><strong>Nome do usuário:</strong> {el.username}</p>
                        {/* <p className="card-body m-0 p-1"><strong>Senha:</strong> {el.password}</p> */}
                        <div className="mx-auto">
                            <button className="btn btn-info"><Link to={`users/${el._id}`} className="text-white">Alterar Informações </Link></button>
                            <button className="ml-3 btn btn-danger" onClick={deleteUserHandle(el._id, el.username)}>Deletar Usuário</button>
                        </div>
                    </div>
                </div>
            )
            )}
        </div>
    )
}

export default UserCard